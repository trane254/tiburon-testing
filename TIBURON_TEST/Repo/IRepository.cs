﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIBURON_TEST.Entities;
using TIBURON_TEST.Models;

namespace TIBURON_TEST.Repo
{
    public interface IRepository
    {
        Task SaveResultAsync(int questionId, int answerId);
        Task<int> GetNextQuestionId(int questionId);
        Task<QuestionModel> GetQuestionByIdAsync(int id);
    }
}
