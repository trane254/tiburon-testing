﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIBURON_TEST.Context;
using TIBURON_TEST.Entities;
using Microsoft.EntityFrameworkCore;
using TIBURON_TEST.Models;
using AutoMapper;

namespace TIBURON_TEST.Repo
{
    public class Repository : IRepository
    {
        private readonly DatabaseContext _context;
        private readonly IMapper _mapper;
        public Repository(DatabaseContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<int> GetNextQuestionId(int questionId)
        {
            var survey = await _context.Surveys
                .AsNoTracking()
                .Include(x => x.Questions)
                .FirstOrDefaultAsync(x => x.Questions.Contains(new Question() { Id = questionId }));

            var next = survey.Questions
                .SkipWhile(x => x.Id <= questionId)
                .ToList();

            if (next.Count != 0)
            {
                return next[0].Id;
            }
            else
            {
                return -1;
            }
        }

        public async Task<QuestionModel> GetQuestionByIdAsync(int id)
        {
            var question = await _context.Questions
                .AsNoTracking()
                .Include(x => x.Answers)
                .FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<QuestionModel>(question);
        }

        public async Task SaveResultAsync(int questionId, int answerId)
        {
            var result = new Result() { QuestionId = questionId, AnswerId = answerId };
            _context.Results.Add(result);

            await _context.SaveChangesAsync();
        }
    }
}
