﻿namespace TIBURON_TEST.Models
{
    public class AnswerModel
    {
        public int Id { get; set; }
        public string AnswerVariant { get; set; }
    }
}
