using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TIBURON_TEST.Context;
using TIBURON_TEST.Entities;
using TIBURON_TEST.Models;
using TIBURON_TEST.Repo;

namespace TIBURON_TEST
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>();

            services.AddAutoMapper(x => 
            {
                x.CreateMap<Answer, AnswerModel>()
                    .ForMember(x => x.AnswerVariant, s => s.MapFrom(x => x.Variant))
                    .ForMember(x => x.Id, s => s.MapFrom(x => x.Id));

                x.CreateMap<Question, QuestionModel>()
                    .ForMember(x => x.Id, s => s.MapFrom(x => x.Id))
                    .ForMember(x => x.QuestionText, s => s.MapFrom(x => x.QuestionText))
                    .ForMember(x => x.Answers, s => s.MapFrom(x => x.Answers));
            });

            services.AddTransient<IRepository, Repository>();
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
