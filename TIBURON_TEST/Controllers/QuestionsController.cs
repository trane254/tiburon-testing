﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIBURON_TEST.Context;
using TIBURON_TEST.Entities;
using TIBURON_TEST.Repo;

namespace TIBURON_TEST.Controllers
{
    [ApiController]
    [Route("api/v1")]
    public class QuestionsController : ControllerBase
    {
        private readonly IRepository _repo;
        public QuestionsController(IRepository repo)
        {
            _repo = repo;
        }

        [HttpGet("question/{id}")]
        public async Task<IActionResult> GetQuestionById(int id)
        {
            var questions = await _repo.GetQuestionByIdAsync(id);

            if (questions is null)
                return BadRequest();

            return new JsonResult(questions);
        }

        [HttpPost("question")]
        public async Task<IActionResult> GetNextQuestionId(int questionId, int answerId)
        {
            await _repo.SaveResultAsync(questionId, answerId);

            int nextQuestionId = await _repo.GetNextQuestionId(questionId);

            return new JsonResult(new { nextQuestionId } );
        }
    }
}
