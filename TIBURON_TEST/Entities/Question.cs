﻿using System.Collections.Generic;

namespace TIBURON_TEST.Entities
{
    public class Question
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string QuestionText { get; set; }
        public int SurveyId { get; set; }



        public virtual Survey Survey { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<Result> Results { get; set; }
    }
}
