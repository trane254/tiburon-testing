﻿using System.Collections.Generic;

namespace TIBURON_TEST.Entities
{
    public class Answer
    {
        public int Id { get; set; }
        public string Variant { get; set; }
        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
        public virtual ICollection<Result> Results { get; set; }
    }
}
