﻿using System.Collections.Generic;

namespace TIBURON_TEST.Entities
{
    public class Result
    {
        public int Id { get; set; }
        public int AnswerId { get; set; }
        public int QuestionId { get; set; }
        

        public virtual Answer Answer { get; set; }
        public virtual Question Question { get; set; }
    }
}
