﻿using System;

namespace TIBURON_TEST.Entities
{
    public class Interview 
    { 
        public int Id { get; set; }
        public int SurveyId { get; set; }
        public TimeSpan SurveyTime { get; set; }

        public Survey Survey { get; set; }
    }
}
