﻿using Microsoft.EntityFrameworkCore;
using System;
using TIBURON_TEST.Entities;

namespace TIBURON_TEST.Context
{
    public class DatabaseContext : DbContext
    {
        public virtual DbSet<Survey> Surveys { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Answer> Answers { get; set; }
        public virtual DbSet<Interview> Interviews { get; set; }
        public virtual DbSet<Result> Results { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder configuring)
        {
            var connectionString = Environment.GetEnvironmentVariable("MSSqlConnectionString");
            configuring.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Survey>(entity =>
            {
                entity.HasMany(x => x.Interviews)
                      .WithOne(x => x.Survey)
                      .HasForeignKey(x => x.SurveyId)
                      .OnDelete(DeleteBehavior.Cascade);

                entity.HasMany(x => x.Questions)
                      .WithOne(x => x.Survey)
                      .HasForeignKey(x => x.SurveyId)
                      .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Interview>(entity =>
            {
                entity.HasOne(x => x.Survey)
                      .WithMany(x => x.Interviews)
                      .HasForeignKey(x => x.SurveyId)
                      .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.HasOne(x => x.Survey)
                      .WithMany(x => x.Questions)
                      .HasForeignKey(x => x.SurveyId)
                      .OnDelete(DeleteBehavior.Cascade);

                entity.HasMany(x => x.Answers)
                      .WithOne(x => x.Question)
                      .HasForeignKey(x => x.QuestionId)
                      .OnDelete(DeleteBehavior.Restrict);

                entity.HasMany(x => x.Answers)
                      .WithOne(x => x.Question)
                      .HasForeignKey(x => x.QuestionId)
                      .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Answer>(entity =>
            {
                entity.HasOne(x => x.Question)
                      .WithMany(x => x.Answers)
                      .HasForeignKey(x => x.QuestionId)
                      .OnDelete(DeleteBehavior.Cascade);

                entity.HasMany(x => x.Results)
                      .WithOne(x => x.Answer)
                      .HasForeignKey(x => x.AnswerId)
                      .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Result>(entity =>
            {
                entity.HasOne(x => x.Question)
                      .WithMany(x => x.Results)
                      .HasForeignKey(x => x.QuestionId);

                entity.HasOne(x => x.Answer)
                       .WithMany(x => x.Results)
                       .OnDelete(DeleteBehavior.Restrict)
                       .HasForeignKey(x => x.AnswerId);
            });
        }
    }
}
