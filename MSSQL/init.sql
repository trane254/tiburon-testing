CREATE DATABASE Tiburon;

GO

USE Tiburon;

CREATE TABLE Surveys(
	Id INT NOT NULL PRIMARY KEY,
	Name NVARCHAR(max),
	Description NVARCHAR(MAX)
)

CREATE TABLE Interviews(
	Id INT NOT NULL PRIMARY KEY,
	SurveyId INT NOT NULL,
	SurveyTime TIME(7) NOT NULL,
	CONSTRAINT FK_Interviews_To_Survey FOREIGN KEY (SurveyId) REFERENCES Surveys (Id) ON DELETE CASCADE 
)

CREATE TABLE Questions(
	Id INT NOT NULL PRIMARY KEY,
	Name NVARCHAR(max),
	QuestionText NVARCHAR(max),
	SurveyId INT NOT NULL,
	CONSTRAINT FK_Questions_To_Surveys FOREIGN KEY (SurveyId) REFERENCES Surveys (Id) ON DELETE CASCADE
)

CREATE TABLE Answers(
	Id INT NOT NULL PRIMARY KEY,
	Variant NVARCHAR(max),
	QuestionId INT NOT NULL,
	CONSTRAINT FK_Answer_To_Question FOREIGN KEY (QuestionId) REFERENCES Questions (Id) ON DELETE CASCADE 
)

CREATE TABLE Results(
	Id INT NOT NULL PRIMARY KEY,
	AnswerId INT NOT NULL,
	QuestionId INT NOT NULL,
	CONSTRAINT FK_Result_To_Answer FOREIGN KEY (AnswerId) REFERENCES Answers (Id) ON DELETE CASCADE,
	CONSTRAINT FK_Result_To_Question FOREIGN KEY (QuestionId) REFERENCES Questions (Id) ON DELETE NO ACTION
)

INSERT INTO Surveys (Id, Name, Description) VALUES (1, 'Testing', 'Toooooooooo teeeeeeeeeeeeeeeeeeeest')